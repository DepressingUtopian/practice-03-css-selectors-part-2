function toggleOnOverlay() {
    var overlay = document.querySelector("body");
    overlay.style.overflow = "hidden";
}

function toggleOffOverlay() {
    var overlay = document.querySelector("body");
    overlay.style.overflow = "auto";
}